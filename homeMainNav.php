<div id="circleContainer">

	<a class="noDecoration" href="send.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">
				Send
			</p>
			<img src="images/icon-send.png">
			<div class="circleChoiceDesc">
				Send to anyone with an email address
			</div>
		</div>
	</a>

	<a class="noDecoration" href="receive.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">
				Receive
			</p>
			<img src="images/icon-receive.png">
			<div class="circleChoiceDesc">
				Send authorization to receive from a third party
			</div>
		</div>
	</a>
	<a class="noDecoration" href="admin.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">Configure</p><img src="images/icon-configure.png">
			<div class="circleChoiceDesc">
				Configure users and other system settings
			</div>
		</div>
	</a>

	<a class="noDecoration" href="audit.php">
		<div class="circleChoice">
			<p class="circleChoiceHeader">
				audit
			</p>
			<img src="images/icon-audit.png">
			<div class="circleChoiceDesc">
				Review transaction and access history
			</div>
		</div>
	</a>

</div>