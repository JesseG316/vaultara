<!DOCTYPE html>
<html class="send-background">
<head>

	<title><?php echo $title; ?></title>
	<?php
		$title = 'Send';
		include 'partials/head.php';
	?>

</head>
<body>
<div id="container">
	<?php
		include 'partials/navbar.php';
	?>

	<div class="banner-background">
		<div id="companyBanner" class="send-banner">

			<img id="send-logo-image" src="images/glowgo.png">

		</div>
	</div>

	<?php
		if (isset($_GET['step'])) {
			switch($_GET['step']) {
				case 4:
					include 'partials/send-step-4.php';
					break;
				case 3:
					include 'partials/send-step-3.php';
					break;
				case 2:
					include 'partials/send-step-2.php';
					break;
				default:
					include 'partials/send-step-1.php';
					break;
			}
		}
		else {
			include 'partials/send-step-1.php';
		}
	?>

	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>