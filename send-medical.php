<!DOCTYPE html>
<html class="send-medical-background">
  <head>
    <?php
        include 'partials/head.php';
      ?>
  </head>

  <body>
    <div id="container">

      <?php
        include 'partials/navbar.php';
      ?>

      <div class="banner-background">
        <div id="companyBanner" class="send-banner">

          <img id="send-logo-image" src="images/glowgo.png">

       </div>
      </div>

      <div class="send-container">
        <div class="send-header">
        <h2 class="header-text" ><img id="send-icon" src="images/icon-send.png"></img>Send Medical Data</h2>

        <div class="send-medical-sub">
          <h5 class="send-medical-select">Select Data Source</h5>
          <p class="send-medical-select2">(reports, JPEG files, etc.), enter recipient's email, and set retenion.</p>
        </div>

        <div class="send-medical-container">
          <form action="SEND">
            <div class="select-file">
              <h5>Select File(s)</h5>
              <a href="#" ><div class="step-circle"><p>+</p></div></a>
            </div>
            <input id="input-name" class="send-input" type="text"  placeholder="To (Email)">
            <input id="datepicker" class="send-input" type="text" placeholder="Expiration Date (YYYY-MM-DD)">
            <button id="send-button" name="form[send-button]" type="submit">SEND</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>