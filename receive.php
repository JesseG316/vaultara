<!DOCTYPE html>
<html class="receive-background">
  <head>
    <?php
    include 'partials/head.php';
    ?>
  </head>

  <body>
    <div id="container">
      <?php
        include 'partials/navbar.php';
      ?>

      <div class="banner-background">
        <div id="companyBanner" class="send-banner">

          <img id="send-logo-image" src="images/glowgo.png">

        </div>
      </div>
      <div class="receive">
        <div class="receive-container">
          <div class="receive-header">

            <div class="header-text" id="receive-text-head">
              <img id="receive-icon" src="images/icon-receive.png"></img>
              <div class="receive-header-text">Authorize Receipt of Medical Data</div>
            </div>

          </div>

          <div class="receive-body">
            <h5 class="receive-content-text">Authorize a third party to upload data by entering their email only </h5>

            <form>
              <input type="text" class="receive-input" required placeholder="Email address">


              <div class="receive-authorize">
                <a href="#" class="receive-authorize-text">Authorize expiration date
                <img id="receive-arrow" src="images/icon-arrow.png"></a>
              </div>
            </form>

          </div>
        </div>
      </div>
    <?php
      include 'footer.php';
    ?>
    </div>

  </body>
</html>