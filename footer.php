 <?php date_default_timezone_set('UTC'); ?>
<div id="footer">
	<span class="social">
		Enterprise 2.0. Copyright&copy; 2014 - <?php echo date('Y');?>. All rights reserved.
	
		<a href="https://www.facebook.com/vaultara"><img height="32" width="32" src="images/social-facebook.png"></a>
		<a href="https://twitter.com/vaultara"><img height="32" width="32"  src="images/social-twitter.png"></a>
	</span>
</div>
