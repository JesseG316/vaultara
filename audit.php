<?php

  $items = [
    [
      "head" => "Tx155 : Share images submitted by Dave Monk (viewable until 2015-3-12 16:08:57)",

      $patients = [
        "PHENIX, MRN : Vafk, ACN::",
        "T.MRN. : Lafk, ACM>",
        "Anonymized.MRN :0: ACN:"
      ],
      $emails = [
        "d4even@gmail.com",
        "dave.monk.vaultary@outlook.com"
      ],
      $accesses = [
        "(2015-1-12 17.19.13) paulson.rob@gmail.com viewed this transaction online via 103.188.65.14",
        "(2009-2-29 14.15.12) anonymous@anon.com viewed this transaction online via 103.188.65.14"
      ]
    ],

    [
      "head" => "Tx155 : Share images submitted by Dave Monk (viewable until 2015-3-12 16:08:57)",

      $patients = [
        "PHENIX, MRN : Vafk, ACN::",
        "T.MRN. : Lafk, ACM>",
        "Anonymized.MRN :0: ACN:"
      ],
      $emails = [
        "d4even@gmail.com",
        "dave.monk.vaultary@outlook.com"
      ],
      $accesses = [
        "(2015-1-12 17.19.13) paulson.rob@gmail.com viewed this transaction online via 103.188.65.14",
        "(2009-2-29 14.15.12) anonymous@anon.com viewed this transaction online via 103.188.65.14"
      ]
    ],

    [
      "head" => "Tx155 : Share images submitted by Dave Monk (viewable until 2015-3-12 16:08:57)",

      $patients = [
        "PHENIX, MRN : Vafk, ACN::",
        "T.MRN. : Lafk, ACM>",
        "Anonymized.MRN :0: ACN:"
      ],
      $emails = [
        "d4even@gmail.com",
        "dave.monk.vaultary@outlook.com"
      ],
      $accesses = [
        "(2015-1-12 17.19.13) paulson.rob@gmail.com viewed this transaction online via 103.188.65.14",
        "(2009-2-29 14.15.12) anonymous@anon.com viewed this transaction online via 103.188.65.14"
      ]
    ],

  ];

?><!DOCTYPE html>


<html class="audit-background">
  <head>
  <?php
    include 'partials/head.php';
  ?>

  </head>
  <body>
    <div id="container">
      <?php
        include 'partials/navbar.php';
      ?>

      <div class="banner-background">
        <div id="companyBanner" class="send-banner">

          <img id="send-logo-image" src="images/glowgo.png">

        </div>
      </div>
      <div class="audit">
        <div class="audit-container">
          <div class="audit-header">
            <div class="audit-header-title" >
              <img id="audit-icon" src="images/icon-audit.png"></img>
              <div class="audit-header-text">Audit</div>
            </div>
          </div>

          <?php foreach($items as $itemIndex => $item): ?>
          <div class="audit-body">
            <div class="audit-drawer">
              <div class="audit-content-head">
                <h2 class="audit-content-head-title"> <?php echo $item["head"]; ?></h2>
                <div class="audit-circle" data-toggle="#content-item-<?php echo $itemIndex; ?>">
                <h2 id="circle-symbol">-</h2><h2 id="circle-symbol2">+</h2></div>
              </div>

              <div class="audit-content" id="content-item-<?php echo $itemIndex; ?>">
                <h2 id="audit-content-title">Study Information (viewable online until 2015-03-12 16:08:57</h2>
                <p id="audit-content-info">Patient: <?php echo $patients[[0][0]]; ?></p>
                <p id="audit-content-info">Patient: <?php echo $patients[[1][0]]; ?></p>
                <p id="audit-content-info">Patient: <?php echo $patients[[2][0]]; ?></p>


                <h2 id="audit-content-title">Recipient Information</h2>
                <p id="audit-content-info"><?php echo $emails [[0][0]]; ?></p>
                <p id="audit-content-info"><?php echo $emails [[1][0]]; ?></p>


                <h2 id="audit-content-title">Access Details</h2>
                <p id="audit-content-info-alt"><?php echo $accesses[[0][0]]; ?></p>
                <p id="audit-content-info-alt"><?php echo $accesses[[1][0]]; ?></p>

              </div>
            </div>
            <?php endforeach; ?>

          </div>

        </div>
      </div>

      <?php
        include 'footer.php'
      ?>

    </div>
  </body>
</html>
