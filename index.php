<!DOCTYPE html>

<html class="index-background">
<head>
	<title>Login</title>
	<?php
		include 'partials/head.php';
	?>
</head>
<body>
<div id="container">
	<div id="login">
		<!-- <p id="vaultaraLogo" class="login">vaultara
		</p> -->
		<div class="logo-container">
			<img id="logo-image" src="images/logo-vault.png">
		</div>
		<div id="productName"> Enterprise DM
				<img id="productIcon" src="images/plane-icon-31x29.png">

		</div>
		<div class="login-msg-container">
			<p class="login">Please log in:</p>
		</div>
		<form action="home.php" method="POST" id="form-id">
			<div id="inputUser">
				<input type="text" name="email" required placeholder="User Name">
			</div>
			<div id="inputPassword">
				<input type="password" name="password" required placeholder="Password">
				<button class="button1" type="submit">
					<span class="fa fa-arrow-right fa-2x"><!--[if IE 7]>><![endif]--> </span>
				</button>
			</div>
		</form>
		<div class="recovery-container">
			<p class="pswd-recovery right"><a href="resendPassword.php">Forgotten password?</a></p>
		</div>
	</div>
	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>
