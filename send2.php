<?php
		include 'partials/send2_data.php';
?>

<!DOCTYPE html>
<html class="send-background">
<head>

	<title>Send</title>

	<?php
		include 'partials/head.php';
	?>


</head>
<body>
<div id="container">
	<?php
		include 'partials/navbar.php';
	?>

	<div class="banner-background">
		<div id="companyBanner" class="send-banner">

			<img id="send-logo-image" src="images/glowgo.png">

		</div>
	</div>

	<div class="send-container">
		<div class="send-header">
			<h2 class="header-text" id="send-head" ><img id="send-icon" src="images/icon-send.png"></img>Send Images</h2>
		</div>

		<div class="send-subheader step2-subhead">
			<div class="subheader-left-item">
				<span class="step-text">STEP</span>
				<div class="step-circle">1</div>
				<img id="step-checked-img" src="images/step-check.png">
			</div>
			<div class="subheader-center-item">
				<span class="step-text">STEP</span>
				<div class="step-circle">2</div>
				<span class="send-filter-text">SELECT STUDIES</span>
				<span class="instruction-text">Use our FLARE wizard to send your image(s) to anyone with an email address in three quick steps.</span>
			</div>
			<div class="subheader-right-item">
				<span class="step-text">STEP</span>
				<div class="step-circle">3</div>
			</div>
		</div>

		<div class="form-container send-2-container">
			<form class="data-form" action="send3.php" >
				<div class="table-container">
					<table class="data-table">
						<thead>
							<tr class="header-row">
								<th class="filler-col"></th>
								<th>PATIENT</th>
								<th>CLIENT ID</th>
								<th>ACCESSION NUMBER</th>
								<th>MODALITY</th>
								<th>STUDY DATE</th>
								<th class="filler-col"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($items as $itemIndex => $item): ?>

								<tr class="data-row" id="data-row">
									<td class="filler-col"></td>
									<td><input class="check-input" type="checkbox" name="selected" value="<?php echo $item['name']; ?>" id="item-<?php echo $itemIndex; ?>" />
											<div class="checkboxRow" for="item-<?php echo $itemIndex; ?>"></div><?php echo $item['name']; ?></td>
									<td><?php echo $item['client_id']; ?></td>
									<td><?php echo $item['accession_no']; ?></td>
									<td><?php echo $item['modality']; ?></td>
									<td><?php echo $item['study_date']; ?></td>
									<td class="filler-col"></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot class="table-footer">
							<tr class="footer-row">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="button-form-wrapper">
					<button class="submit-data" type="submit">SUBMIT</button>
				</div>
			</form>

		</div>

	</div>

	<?php
		include 'footer.php';
	?>
</div>
</body>
</html>