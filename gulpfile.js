var gulp = require('gulp')
	, plumber = require('gulp-plumber')
	, stylus = require('gulp-stylus')
	, autoprefix = require('gulp-autoprefixer')
	, minifyCss = require('gulp-minify-css')
	, src = './src'
	, dist = './'


gulp.task('styles', function () {
	return gulp
		.src(src + '/styles/*.styl')
		.pipe(plumber())
		.pipe(stylus())
		.pipe(autoprefix())
		.pipe(gulp.dest(dist + '/css'))
})

gulp.task('watch', ['build'], function () {
	gulp.watch(src + '/styles/**/*.styl', ['styles'])
})

gulp.task('build', ['styles'])

gulp.task('default', ['build', 'watch'])